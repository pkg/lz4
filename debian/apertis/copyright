Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2013-2025, Nobuhiro Iwamatsu <iwamatsu@debian.org>
License: GPL-2+

Files: build/meson/*
Copyright: 2011-2020, Yann Collet
License: GPL-2+

Files: Makefile
 Makefile.inc
Copyright: 2011-2023, Yann Collet
License: BSD-2-clause

Files: build/meson/meson.build
Copyright: no-info-found
License: BSD-2-Clause-Patent

Files: contrib/djgpp/*
Copyright: 2014, lpsantil
License: BSD-2-clause

Files: contrib/gen_manual/Makefile
Copyright: 2016-2025, Przemyslaw Skibinski
License: BSD-2-clause

Files: contrib/gen_manual/gen_manual.cpp
Copyright: no-info-found
License: BSD-2-clause

Files: examples/*
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: GPL-2

Files: examples/Makefile
Copyright: 2011-2024, Yann Collet
License: GPL-2+

Files: lib/*
Copyright: 2011-2023, Yann Collet
License: BSD-2-clause

Files: lib/dll/example/Makefile
Copyright: 2011-2024, Yann Collet
License: GPL-2+

Files: lib/lz4.c
 lib/lz4.h
 lib/lz4frame.c
 lib/lz4frame.h
 lib/lz4frame_static.h
 lib/lz4hc.c
 lib/lz4hc.h
 lib/xxhash.h
Copyright: 2011-2023, Yann Collet.
License: BSD-2-clause

Files: lib/lz4file.c
 lib/lz4file.h
Copyright: 2022, Xiaomi Inc.
License: BSD-2-clause

Files: programs/*
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: GPL-2

Files: programs/Makefile
 programs/bench.c
 programs/bench.h
 programs/lorem.c
 programs/lorem.h
 programs/lz4cli.c
 programs/lz4conf.h
 programs/lz4io.c
 programs/lz4io.h
 programs/threadpool.c
 programs/threadpool.h
 programs/timefn.c
 programs/timefn.h
 programs/util.c
Copyright: 2011-2024, Yann Collet
License: GPL-2+

Files: programs/platform.h
 programs/util.h
Copyright: 2016-2023, Przemyslaw Skibinski, Yann Collet
License: GPL-2+

Files: tests/*
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: GPL-2

Files: tests/Makefile
 tests/checkFrame.c
 tests/checkTag.c
 tests/datagen.c
 tests/datagen.h
 tests/datagencli.c
 tests/frametest.c
 tests/fullbench.c
 tests/fuzzer.c
 tests/loremOut.c
 tests/loremOut.h
Copyright: 2011-2024, Yann Collet
License: GPL-2+

Files: build/VS2022/liblz4-dll/liblz4-dll.rc build/VS2022/lz4/lz4.rc doc/lz4_Frame_format.md ossfuzz/fuzz_helpers.h programs/lz4-exe.rc.in tests/test-lz4-abi.py tests/test-lz4-versions.py
Copyright: 2011-2020, Yann Collet
License: GPL-2+

Files: contrib/djgpp/Makefile
Copyright: 2015, Louis P. Santillan <lpsantil@gmail.com>
License: BSD-2-clause

Files: examples/bench_functions.c examples/simple_buffer.c
Copyright: Kyle Harper
License: GPL-2+

Files: examples/bench_functions.c examples/simple_buffer.c
Copyright: Kyle Harper
License: BSD-2-clause

Files: examples/blockStreaming_doubleBuffer.c
Copyright: Takayuki Matsuoka
License: BSD-2-clause

Files: lib/liblz4-dll.rc.in lib/liblz4.pc.in
Copyright: 2011-2023, Yann Collet
License: BSD-2-clause

Files: tests/abiTest.c tests/roundTripTest.c
Copyright: 2016-2020, Yann Collet, Facebook, Inc.
License: BSD-2-clause or GPL-2+

Files: tests/test-lz4-speed.py
Copyright: 2016-2020, Przemyslaw Skibinski, Yann Collet, Facebook, Inc.
License: BSD-2-clause
